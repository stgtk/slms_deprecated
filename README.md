# Stugütak Live Media System

Creating a media playback and live video system for theatrical productions and alike. This project should not only propagate open source in the field of theatre tech, slms aims to enable the use of live video in independent, «low-cost» (in contrast to the state funded houses) theatre productions. The project was initially created 2019 for the Play «RAGNA::ROEK».


## Aims

- Producing a replicable and cost-aware solution for live streaming video in stage plays.
- Documenting the setup and usage of the software. So other artists can adopt it.
- Do not reinvent the wheel. Whenever possible, we use existing solutions which are field tested.
- The implementation should be as agnostic as possible, so there is room for further development.


## Architecture Overview

Slms takes control input from en interface and triggers Targets accordingt to a given cue list. The video and audio pipeline is heavily based on the excellent [c3voc voctomix](https://github.com/voc/voctomix).

### Hardware

### Software
The core («Server») of slms takes commands trough OSC ([Open Sound Control](https://en.wikipedia.org/wiki/Open_Sound_Control)) and propagates this to the according targets. The flow of a show is set in a json file. The application's architecture is based on a [Actor model](https://en.wikipedia.org/wiki/Actor_model) which is implemented trough the [Proto Actor Framework](https://github.com/AsynkronIT/protoactor-go). Here is a schematic overview of the internal structure: 

![Schematic overview of the software structure](doc/assets/structure.png)

- `interface` Actors, which receive control input.
- `cmd` An executable start point of the application. This enables the easy reuse of targets and or other components.
- `target` An actor which can execute an Cue. For we need the following targets.
- `service` Provides some functionality for the Target.
- `handler` Handles and control external resources/processes like ffmpeg instances.
 


## Targets for «RAGNA::ROEK»

For R::R we need to do:

- [ ] Infrastructure. Research and build the hardware we need (Cameras, Cables, Servers, so on)
- [ ] slms. The control software
- [ ] Control Interfaces
    - [ ] OSC Interface
- [ ] Media Player Target
    - [ ] ffmpeg decklink input
    - [ ] ffmpeg video file input
    - [ ] voctocore control
    - [ ] Output
- [ ] Other Targets
    - [ ] Coordinated Playback System («cps»). Play videos on multiple screens with dedicated playback nodes controlled via the network. 
    - [ ] Audio Output Matrix. Control of the audio output channels.
- [ ] Voctomix tweaks
    - [ ] Set alpha to an video input
    - [ ] Composite multiple video sources with positioning and resizing
    - [ ] Mirror effect
- [ ] Json Schemas for cue list and configurations
