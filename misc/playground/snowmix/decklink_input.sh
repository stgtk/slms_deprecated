#!/bin/sh
CONTROL_PIPE=/tmp/feed1-control-pipe
width=1920
height=1080
framerate='50/1'
MIXERFORMAT='video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, interlace-mode=progressive'
SHMOPTION="wait-for-connection=0 sync=true"
SHMSINK1="shmsink socket-path=/tmp/feed1-control-pipe $SHMSIZE $SHMOPTION"
gst-launch-1.0 decklinkvideosrc mode=0 device-number=0 connection=1 name=black ! \
  queue                   !\
  deinterlace             !\
  queue                   !\
  videorate               !\
  queue                   !\
  videoscale method=1     !\
  queue                   !\
  videoconvert ! $MIXERFORMAT           !\
  queue                   !\
  $SHMSINK1
