#!/bin/bash
# These settings are the settings defined for video feed 1 in Snowmix
SNOWMIX=~/Downloads/Snowmix-0.5.1
CONTROL_PIPE=/tmp/feed1-control-pipe
width=640
height=360
framerate='24/1'
gstlaunch=gst-launch-1.0
DECODEBIN=decodebin
SHMSIZE='shm-size='`echo "$width * $height * 4 * 22"|bc`
MIXERFORMAT='video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, interlace-mode=progressive'
SCALENRATE='videoconvert ! videorate ! videoscale ! videoconvert'
SRC="filesrc location=$SNOWMIX/test/LES_TDS_launch.mp4 ! $DECODEBIN "
SHMOPTION="wait-for-connection=0 sync=true"
SHMSINK1="shmsink socket-path=$CONTROL_PIPE $SHMSIZE $SHMOPTION"
while true ; do
    # Remove the named pipe if it exist
    rm -f $CONTROL_PIPE
    $gstlaunch -v   \
        $SRC            !\
        $SCALENRATE     !\
        "$MIXERFORMAT,framerate=$framerate, width=$width, height=$height, format=BGRA"    !\
        $SHMSINK1
    sleep 2
done
exit
