#!/bin/sh
# Deliver mixer1 output to screen.
# The geometry and framerate are from your system settings in Snowmix
width=1920
height=1080
framerate='50/1'
gstlaunch=gst-launch-1.0
VIDEOCONVERT=videoconvert
MIXERFORMAT='video/x-raw, format=BGRA, pixel-aspect-ratio=1/1'
$gstlaunch -v             \
   shmsrc socket-path=/tmp/feed1-control-pipe do-timestamp=true is-live=true      !\
   $MIXERFORMAT", framerate=$framerate, width=$width, height=$height" !\
   queue                !\
   $VIDEOCONVERT        !\
   queue                !\
   autovideosink
