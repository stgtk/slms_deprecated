#!/bin/sh
CONTROL_PIPE=/tmp/feed-1-control-pipe
width=1920
height=1080
framerate='25/1'
gstlaunch=gst-launch-1.0
DECODEBIN=decodebin
SHMSIZE='shm-size='`echo "$width * $height * 4 * 22"|bc`
MIXERFORMAT='video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, interlace-mode=interleaved'
SCALE='deinterlace ! videorate ! videoconvert '
SRC="decklinkvideosrc mode=11 device-number=0 connection=1 name=black "
SHMOPTION="wait-for-connection=0 sync=true "
SHMSINK1="shmsink socket-path=$CONTROL_PIPE $SHMSIZE $SHMOPTION"
while true ; do
    # Remove the named pipe if it exist
    rm -f $CONTROL_PIPE
    $gstlaunch -v          \
        $SRC              !\
        $SCALE            !\
        "$MIXERFORMAT,framerate=$framerate, width=$width, height=$height, format=BGRA"    !\
        $SHMSINK1
    sleep 2
done
exit
