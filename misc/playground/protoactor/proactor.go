package main

import (
	"fmt"
	"github.com/AsynkronIT/goconsole"
	"github.com/AsynkronIT/protoactor-go/actor"
)

type Ffmpeg struct {
	InputName string
}

type FfmpegActor struct{}

func (state *FfmpegActor) Receive(context actor.Context) {
	switch msg := context.Message().(type) {
	case Ffmpeg:
		fmt.Printf("Hoi %v\n", msg.InputName)
	}
}

func main() {
	/*
		props := actor.FromProducer(func() actor.Actor {
			return &FfmpegActor{}
		})
		pid := actor.Spawn(props)
		pid.Tell(Ffmpeg{InputName: "Cam 1"})
		console.ReadLine()
	*/

	props := actor.FromProducer(func() actor.Actor {
		return &MediaPlayerTarget{}
	})
	pid := actor.Spawn(props)
	pid.Tell(Cue{Name: "Video 1 Go"})
	console.ReadLine()
}
