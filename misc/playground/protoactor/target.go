package main

import (
	"fmt"
	"github.com/AsynkronIT/protoactor-go/actor"
)

// Target is a Element which can execute an specific cue.
//
// For Ragna::Roek we need:
// 	- Multimedia Player
// 	- Coordinated Playback System
// 	- Audio Output Matrix
type Target interface {
	actor.Actor
	TearUp(configuration TargetConfiguration) // Function is called, when Target is initialized.
	TearDown()
	Go(cue Cue)
}

type TargetConfiguration struct{}

// ==================================================
// For Prototyping, move this to an other file later

type MediaPlayerTarget struct {
	Name string
}

func (state *MediaPlayerTarget) Receive(context actor.Context) {
	switch msg := context.Message().(type) {
	case Cue:
		fmt.Printf("Got Cue with name %s", msg.Name)
	}
}

func (MediaPlayerTarget) TearUp(configuration TargetConfiguration) {
	fmt.Printf("Tear Up MediaPlayerTarget\n")
}

func (MediaPlayerTarget) TearDown() {
	fmt.Printf("Tear down MediaPlayerTarget\n")
}

func (MediaPlayerTarget) Go(cue Cue) {
	fmt.Printf("This is the Go function\n")
}
