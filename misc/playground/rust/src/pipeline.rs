pub trait GstValue {
    fn render_value(&self) -> String;
    fn set_value();
}

pub struct GstFraction {
    numerator: i32,
    denominator: i32,
}

impl GstFraction {
    pub fn render_value(&self) -> String {
        format!("(fraction){}/{}", self.numerator, self.denominator)
    }
}


pub struct GstString {
    pub value
}