mod pipeline;

fn main() {
    let element = pipeline::GstFraction {
        numerator: 30,
        denominator: 1,
    };

    println!("{}", element.render_value());

}
