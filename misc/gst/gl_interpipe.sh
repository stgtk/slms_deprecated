#!/bin/bash

# GST_DEBUG="GST_TRACER:7" GST_TRACERS="graphic" \
gst-launch-1.0 -v videotestsrc is-live=true ! \
  "video/x-raw,format=RGBA,width=640,height=480,framerate=(fraction)30/1" ! \
  glupload ! \
  "video/x-raw(memory:GLMemory),format=RGBA,width=640,height=480,framerate=30/1" ! \
  interpipesink name=s1 \
  interpipesrc listen-to=s1 format=time is-live=true ! \
  "video/x-raw(memory:GLMemory),format=RGBA,width=640,height=480,framerate=30/1" ! \
  glimagesink
