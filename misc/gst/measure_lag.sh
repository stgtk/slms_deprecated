#!/bin/bash

gst-launch-1.0 -q videotestsrc pattern=black ! \
  'video/x-raw, width=1280, height=720' ! \
  timeoverlay halignment=center valignment=center font-desc="Sans, 44" ! \
  autovideosink &

# Put YOUR COMMAND below here
gst-launch-1.0 -q decklinkvideosrc mode=11 device-number=0 connection=1 name=black ! \
  deinterlace ! videorate ! videoconvert ! \
  'video/x-raw, pixel-aspect-ratio=1/1, interlace-mode=interleaved, framerate=30/1, width=1920, height=1080' ! \
  glupload ! \
  glimagesink &
# Put YOUR COMMAND above here

mkdir /tmp/measure_lag
read -p "Arrange the windows and film the timer. Then press enter"
gnome-screenshot -f /tmp/measure_lag/1.png
sleep 0.2s
gnome-screenshot -f /tmp/measure_lag/2.png
sleep 0.2s
gnome-screenshot -f /tmp/measure_lag/3.png
sleep 0.2s
gnome-screenshot -f /tmp/measure_lag/4.png
sleep 0.2s
gnome-screenshot -f /tmp/measure_lag/5.png
sleep 0.2s
gnome-screenshot -f /tmp/measure_lag/6.png
sleep 0.2s
gnome-screenshot -f /tmp/measure_lag/7.png
killall gst-launch-1.0
nemo /tmp/measure_lag
read -p "Analyse the files. Then press enter"
rm -r /tmp/measure_lag
