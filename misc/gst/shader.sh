#!/bin/bash

gst-launch-1.0 -v decklinkvideosrc mode=11 device-number=0 connection=1 name=black ! \
  deinterlace ! videorate ! videoconvert ! \
  'video/x-raw, pixel-aspect-ratio=1/1, interlace-mode=interleaved, framerate=30/1, width=1920, height=1080' ! \
  glupload ! \
  glshader fragment="\"`cat shader_1.frag`\"" ! \
  glimagesink
