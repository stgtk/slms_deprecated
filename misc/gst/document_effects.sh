#!/bin/bash

effects=("gleffects_blur" "gleffects_bulge" "gleffects_fisheye" "gleffects_glow" "gleffects_heat" "gleffects_identity" "gleffects_laplacian" "gleffects_lumaxpro" "gleffects_mirror" "gleffects_sepia" "gleffects_sin" "gleffects_sobel" "gleffects_square" "gleffects_squeeze" "gleffects_stretch" "gleffects_tunnel" "gleffects_twirl" "gleffects_xpro" "gleffects_xray")

for effect in "${effects[@]}"
do
  gst-launch-1.0 -q decklinkvideosrc mode=11 device-number=0 connection=1 name=black ! \
    deinterlace ! videorate ! videoconvert ! \
    'video/x-raw, pixel-aspect-ratio=1/1, interlace-mode=interleaved, framerate=30/1, width=1920, height=1080' ! \
    glupload ! \
    $effect ! \
    glimagesink &
    # read -p "$effect Press Enter to proceed"
    sleep 0.4s
    gnome-screenshot -w -f /tmp/gl_effects/$effect.png
    sleep 0.1s
    killall gst-launch-1.0
done
