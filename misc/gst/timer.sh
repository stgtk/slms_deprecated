#!/bin/bash

gst-launch-1.0 -v videotestsrc pattern=black ! \
  'video/x-raw, width=1280, height=720' ! \
  timeoverlay halignment=center valignment=center font-desc="Sans, 44" ! \
  autovideosink
