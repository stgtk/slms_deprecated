package Targets

import (
	"github.com/AsynkronIT/protoactor-go/actor"
)

// Target is a Element which can execute an specific cue.
type Target interface {
	actor.Actor
}
