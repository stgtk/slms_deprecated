package MediaPlayer

import (
	"github.com/AsynkronIT/protoactor-go/actor"
	"gitlab.com/stgtk/slms/misc/deprecated/Targetsde/Targets/MediaPlayer/InputService"
	"gitlab.com/stgtk/slms/pkg/Commands"
)

import log "github.com/sirupsen/logrus"

type MediaPlayer struct {
	InputService          InputService.InputService
	VoctocoreConfFilePath string
	Pid                   *actor.PID
}

func (MediaPlayer) GetDefaultConfig() MediaPlayer {
	inputService := InputService.InputService{}
	return MediaPlayer{
		InputService:          inputService.GetDefaultConfig(),
		VoctocoreConfFilePath: "/opt/voctomix/voctocore/config.conf",
	}
}

func (mediaPlayer MediaPlayer) Receive(context actor.Context) {
	switch msg := context.Message().(type) {
	case *actor.Started:
		log.WithField("actor", "mediaPlayer").Trace("Has started")
	case *actor.Stopping:
		log.WithField("actor", "mediaPlayer").Trace("Is stopping")
		log.Debug("Controller is stopping")
	case *actor.Stopped:
		log.WithField("actor", "mediaPlayer").Trace("Has stopped")
		log.Debug("Controller has stopped")
	case *actor.Restarting:
		log.WithField("actor", "mediaPlayer").Trace("Is restarting")
		log.Debug("Controller is restarting")

	case Commands.TearUp:
		log.WithField("actor", "mediaPlayer").Debug("Receive TearUp command")
		context.Respond("ok")

	default:
		log.WithFields(log.Fields{
			"actor":            "mediaPlayer",
			"received_message": msg,
		}).Warn("Controller received unspecified message")
	}
}
