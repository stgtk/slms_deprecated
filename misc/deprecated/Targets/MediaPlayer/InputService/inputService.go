package InputService

import (
	"github.com/AsynkronIT/protoactor-go/actor"
)

import log "github.com/sirupsen/logrus"

type InputService struct {
	DeckLinkInputs []DeckLinkInput
	FileInputs     []FileInput
}

func (InputService) GetDefaultConfig() InputService {
	deckLinkInput := DeckLinkInput{}
	fileInput := FileInput{}

	return InputService{
		DeckLinkInputs: []DeckLinkInput{
			deckLinkInput.GetDefaultConfig(),
		},
		FileInputs: []FileInput{
			fileInput.GetDefaultConfig(),
		},
	}
}

type DeckLinkInput struct {
	Id          string
	Name        string
	Port        int
	InputString string
	VideoCodec  string
	AudioCodec  string
	PixFmt      string
	Container   string
}

func (DeckLinkInput) GetDefaultConfig() DeckLinkInput {
	return DeckLinkInput{
		Id:          "c1",
		Name:        "Camera 1",
		Port:        10000,
		InputString: "DeckLink Mini Recorder (1)@10",
		VideoCodec:  "rawvideo",
		AudioCodec:  "pcm_s16le",
		PixFmt:      "yuv420p",
		Container:   "matroska",
	}
}

type FileInput struct {
	Id   string
	Name string
	Port int
	Path string
}

func (FileInput) GetDefaultConfig() FileInput {
	return FileInput{
		Id:   "f1",
		Name: "Video Source 1",
		Port: 10004,
	}
}

func (FileInput) Receive(contex actor.Context) {
	switch msg := contex.Message().(type) {
	case *actor.Started:
		log.Debug("FileInput has started")
	case *actor.Stopping:
		log.Debug("FileInput is stopping")
	case *actor.Stopped:
		log.Debug("FileInput has stopped")
	case *actor.Restarting:
		log.Debug("FileInput is restarting")
	default:
		log.WithField("received_message", msg).Info("FileInput received unspecified message")
	}
}
