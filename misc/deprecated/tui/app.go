package main

import (
	"github.com/urfave/cli"
)

func appFactory() *cli.App {

	app := cli.NewApp()
	app.Name = "slms_tui"
	app.HelpName = "slms_tui"
	app.Usage = "Controls the Stgtk Live Media Server"
	app.Author = "Produktionskollektiv Stugütak Bern"
	app.Email = "info@stugütak.ch"
	app.Version = "0.1.0"

	app.Commands = []cli.Command{
		{
			Name:   "config",
			Usage:  "Configure a show",
			Action: runConfigMode,
		},
		{
			Name:   "cue",
			Usage:  "Runs in Cue Mode",
			Action: runCueMode,
		},
		{
			Name:   "generate_showfile",
			Usage:  "Generates and saves a example showfile",
			Action: generateShowFile,
		}}

	return app
}
