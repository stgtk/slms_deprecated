package ConfigMode

import "gitlab.com/stgtk/slms/misc/deprecated/tuiprecated/tui/Common"

func uiCommandsFactory() Common.UiCommands {
	return Common.UiCommands{
		Common.UiCommand{
			Name:            "quit",
			Description:     "Quit slms",
			Key:             "q",
			SimpleCommand:   true,
			OnEventFunction: nil,
		},
		Common.UiCommand{
			Name:            "up",
			Description:     "Previous Element",
			Key:             "<Up>",
			SimpleCommand:   true,
			OnEventFunction: nil,
		},
		Common.UiCommand{
			Name:            "down",
			Description:     "Next Element",
			Key:             "<Down>",
			SimpleCommand:   true,
			OnEventFunction: nil,
		},
		Common.UiCommand{
			Name:            "tab",
			Description:     "Change Context",
			Key:             "<Tab>",
			SimpleCommand:   true,
			OnEventFunction: nil,
		},
		Common.UiCommand{
			Name:            "set",
			Description:     "Set Value to Element",
			Key:             "s",
			Format:          "<cmd><type:any,name:value><enter>",
			SimpleCommand:   false,
			OnEventFunction: nil,
		},
		Common.UiCommand{
			Name:            "test",
			Description:     "Test for complex command",
			Key:             "t",
			Format:          "<cmd><type:choice,name:type,values:(i:interpipes,v:videofiles)><enter>",
			SimpleCommand:   false,
			OnEventFunction: nil,
		},
	}
}
