package ConfigMode

import (
	"github.com/gizak/termui"
	"gitlab.com/stgtk/slms/misc/deprecated/tuiprecated/tui/Common"
	"time"
)

import log "github.com/sirupsen/logrus"

var header *Common.Header
var sidebar *ConfigSidebar
var buffer *Common.Buffer

func initUi() {
	header = Common.NewHeader("Config Mode")
	header.Update(termui.Render)

	sidebar = sidebarFactory()
	termui.Render(sidebar)

	buffer = Common.NewBuffer()
	buffer.Update(termui.Render)

	buildLayout()
}

func buildLayout() {
	termui.Body.AddRows(
		termui.NewRow(
			termui.NewCol(12, 0, header)),
		termui.NewRow(
			termui.NewCol(5, 0, sidebar)),
		termui.NewRow(
			termui.NewCol(12, 0, buffer)),
	)
}

func Run() error {
	err := termui.Init()
	if err != nil {
		log.Error(err)
	}
	defer termui.Close()

	initUi()
	termui.Body.Align()
	termui.Render(termui.Body)

	uiEvent := termui.PollEvents()
	updateTicker := time.NewTicker(time.Second).C

	for {
		select {
		case e := <-uiEvent:
			switch e.ID {
			case "q":
				return nil
			default:
				// buffer.SetStatus(uiCommands.ParseKeyEventAndRunCommand(e.ID), termui.Render)
			}
		case <-updateTicker:

		}
	}

	return nil
}
