package ConfigMode

import (
	"github.com/gizak/termui"
	"gitlab.com/stgtk/slms/misc/deprecated/tuiprecated/tui/Common"
)

type ConfigSidebar Common.Sidebar

func sidebarFactory() *ConfigSidebar {
	sidebar := &ConfigSidebar{
		Paragraph: *termui.NewParagraph(""),
	}
	sidebar.Context = sidebar.objectContextFactory()
	sidebar.Height = 30

	return sidebar
}

func (sidebar *ConfigSidebar) update() {
	sidebar.Text = sidebar.Context.Content()
	termui.Render(sidebar)
}

func (sidebar *ConfigSidebar) changeContext(context Common.SidebarContext) {
	sidebar.Context = context
	sidebar.BorderLabel = context.Name
	sidebar.Text = context.Content()
	sidebar.update()
}

// OBJECT CONTEXT

func objectContent() string {
	return "hoi"
}

func (sidebar *ConfigSidebar) objectContextFactory() Common.SidebarContext {
	return Common.SidebarContext{
		Name:    "Objects",
		Content: objectContent,
	}
}

func (sidebar *ConfigSidebar) changeToInfoContext() {
	sidebar.changeContext(sidebar.objectContextFactory())
}
