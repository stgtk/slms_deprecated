package Common

import (
	"fmt"
	"github.com/gizak/termui"
)

type Buffer struct {
	termui.Paragraph
	Status    string
	InputLine string
}

func NewBuffer() *Buffer {
	buffer := &Buffer{
		Paragraph: *termui.NewParagraph(""),
		Status:    "Welcome to the slms console! For help type 'h'.",
		InputLine: "",
	}
	buffer.Height = 4
	return buffer
}

func (buffer *Buffer) Update(renderFn render) {
	buffer.Text = fmt.Sprintf("[> %s](fg-blue)\n> %s", buffer.Status, buffer.InputLine)
	renderFn(buffer)
}

func (buffer *Buffer) SetStatus(newStatus string, renderFn render) {
	buffer.Status = newStatus
	buffer.Update(renderFn)
}
