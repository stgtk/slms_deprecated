package Common

import "github.com/gizak/termui"

type Sidebar struct {
	termui.Paragraph
	Context SidebarContext
	Update  func()
}

type SidebarContext struct {
	Name    string
	Content func() string
	Update  func()
}
