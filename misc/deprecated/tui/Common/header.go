package Common

import (
	"fmt"
	"github.com/gizak/termui"
)

type Header struct {
	termui.Paragraph
	Title    string
	SubTitle string
	Mode     string
}

func NewHeader(mode string) *Header {
	header := &Header{
		Paragraph: *termui.NewParagraph(""),
		Title:     "SLMS CONSOLE",
		SubTitle:  "Stgtk Live Media System, Version 0.1",
		Mode:      mode,
	}
	header.Height = 3
	return header
}

func (header *Header) Update(renderFn render) {
	header.Text = fmt.Sprintf("[%s](fg-bold) – [%s](fg-underline) – %s", header.Title, header.Mode, header.SubTitle)
	renderFn(header)
}

func (header *Header) setMode(newMode string, renderFn render) {
	header.Mode = newMode
	header.Update(renderFn)
}
