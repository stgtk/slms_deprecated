package Common

import (
	"fmt"
)

type CommandFunction func()

type UiCommand struct {
	Name            string
	Description     string
	Key             string
	Format          string
	SimpleCommand   bool
	OnEventFunction CommandFunction
}

type UiCommands []UiCommand

func (commands UiCommands) ParseKeyEventAndRunCommand(eventId string) (userInformation string) {
	for _, command := range commands {
		if eventId == command.Key {
			if command.SimpleCommand {
				command.OnEventFunction()
				return fmt.Sprintf("'%s' executed.", command.Name)
			}
		}
	}
	return fmt.Sprintf("Key «%s» has no functionality", eventId)
}

func (command *UiCommand) ParseComplexCommand() {
	fmt.Println("Implement that!")
}
