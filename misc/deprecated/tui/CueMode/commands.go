package CueMode

import (
	"gitlab.com/stgtk/slms/misc/deprecated/tuiprecated/tui/Common"
)

func helpCommand() {
	defaultSidebar.changeToHelpContext()
}

func infoCommand() {
	defaultSidebar.changeToInfoContext()
}

func selectCueAbove() {
	defaultCueList.selectCueAbove()
	defaultSidebar.update()
}

func selectCueBelow() {
	defaultCueList.selectCueBelow()
	defaultSidebar.update()
}

func goCommand() {
	defaultCueList.goToNextCue()
	defaultSidebar.update()
}

func jumpCommand() {
	defaultCueList.jumpToSelectedCue()
	defaultSidebar.update()
}

func previousCueCommand() {
	defaultCueList.goToPreviousCue()
	defaultSidebar.update()
}

func uiCommandsFactory() Common.UiCommands {
	return Common.UiCommands{
		Common.UiCommand{
			Name:            "quit",
			Description:     "Quit slms",
			Key:             "q",
			SimpleCommand:   true,
			OnEventFunction: nil,
		},
		Common.UiCommand{
			Name:            "help",
			Description:     "This Help",
			Key:             "h",
			SimpleCommand:   true,
			OnEventFunction: helpCommand,
		},
		Common.UiCommand{
			Name:            "info",
			Description:     "Show Info Sidebar",
			Key:             "<C-j>",
			SimpleCommand:   true,
			OnEventFunction: infoCommand,
		},
		Common.UiCommand{
			Name:            "Cue -",
			Description:     "Select Cue Above",
			Key:             "<Up>",
			SimpleCommand:   false,
			OnEventFunction: selectCueAbove,
		},
		Common.UiCommand{
			Name:            "Cue +",
			Description:     "Select Cue Below",
			Key:             "<Down>",
			SimpleCommand:   false,
			OnEventFunction: selectCueBelow,
		},
		Common.UiCommand{
			Name:            "go",
			Description:     "Fire the next cue",
			Key:             "<Space>",
			SimpleCommand:   true,
			OnEventFunction: goCommand,
		},
		Common.UiCommand{
			Name:            "previous cue",
			Description:     "Fires previous cue",
			Key:             "p",
			SimpleCommand:   true,
			OnEventFunction: previousCueCommand,
		},
		Common.UiCommand{
			Name:            "jump",
			Description:     "Jump and fire selected cue",
			Key:             "j",
			SimpleCommand:   true,
			OnEventFunction: jumpCommand,
		}}
}
