package CueMode

import (
	"github.com/gizak/termui"
	"gitlab.com/stgtk/slms/misc/deprecated/tuiprecated/tui/Common"
	"gitlab.com/stgtk/slms/pkg/Data"
	"log"
	"time"
)

var show Data.Show
var uiCommands Common.UiCommands
var defaultHeader *Common.Header
var defaultSidebar *CueSidebar
var defaultCueList *CueList
var defaultBuffer *Common.Buffer

func initUi() {
	uiCommands = uiCommandsFactory()

	defaultHeader = Common.NewHeader("Cue Mode")
	defaultHeader.Update(termui.Render)

	defaultSidebar = sidebarFactory()
	defaultSidebar.changeToInfoContext()

	defaultCueList = newCueList()
	defaultCueList.update()

	defaultBuffer = Common.NewBuffer()
	defaultBuffer.Update(termui.Render)

	buildLayout()
}

func buildLayout() {
	termui.Body.AddRows(
		termui.NewRow(
			termui.NewCol(12, 0, defaultHeader)),
		termui.NewRow(
			termui.NewCol(4, 0, defaultSidebar),
			termui.NewCol(8, 0, defaultCueList)),
		termui.NewRow(
			termui.NewCol(12, 0, defaultBuffer)),
	)
}

func Run(inputShow Data.Show) error {
	err := termui.Init()
	if err != nil {
		log.Panic(err)
	}
	defer termui.Close()
	show = inputShow
	initUi()
	termui.Body.Align()
	termui.Render(termui.Body)

	uiEvent := termui.PollEvents()
	updateTicker := time.NewTicker(time.Second).C

	for {
		select {
		case e := <-uiEvent:
			switch e.ID {
			case "q":
				return nil
			default:
				defaultBuffer.SetStatus(uiCommands.ParseKeyEventAndRunCommand(e.ID), termui.Render)
			}
		case <-updateTicker:

		}
	}

	return nil
}
