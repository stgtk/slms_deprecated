package CueMode

import (
	"fmt"
	"github.com/gizak/termui"
	"gitlab.com/stgtk/slms/misc/deprecated/tuiprecated/tui/Common"
)

// STRUCTS

type CueSidebar Common.Sidebar

// SIDEBAR

func sidebarFactory() *CueSidebar {
	sidebar := &CueSidebar{
		Paragraph: *termui.NewParagraph(""),
	}
	sidebar.Context = sidebar.infoContextFactory()
	sidebar.Height = 30
	sidebar.BorderLabelFg = termui.ColorYellow
	return sidebar
}

func (sidebar *CueSidebar) update() {
	defaultSidebar.Text = defaultSidebar.Context.Content()
	termui.Render(defaultSidebar)
}

func (sidebar *CueSidebar) changeContext(context Common.SidebarContext) {
	sidebar.Context = context
	sidebar.BorderLabel = context.Name
	sidebar.Text = context.Content()
	sidebar.update()
}

// INFO CONTEXT

func infoContent() string {
	general := fmt.Sprintf("[General](fg-underline)\n"+
		" Show: %s", show.Name)

	currentCue := show.CueList[0]

	currentCueInfo := fmt.Sprintf("[Current Cue](fg-underline)\n"+
		" Number: %s\n"+
		" Desc.: %s\n"+
		" Trigger: %s", currentCue.Number, currentCue.Description, currentCue.Trigger)

	return fmt.Sprintf("%s\n\n%s", general, currentCueInfo)
}

func (sidebar *CueSidebar) infoContextFactory() Common.SidebarContext {
	return Common.SidebarContext{
		Name:    "Info",
		Content: infoContent,
	}
}

func (sidebar *CueSidebar) changeToInfoContext() {
	sidebar.changeContext(sidebar.infoContextFactory())
}

// HELP CONTEXT

func helpContent() string {
	var helpText string
	for index, command := range uiCommands {
		prefix := "\n"
		if index == 0 {
			prefix = ""
		}
		helpText = fmt.Sprintf("%s%s[%s](fg-bold) %s", helpText, prefix, command.Key, command.Description)
	}
	return helpText
}

func (sidebar *CueSidebar) helpContextFactory() Common.SidebarContext {
	return Common.SidebarContext{
		Name:    "Help",
		Content: helpContent,
	}
}

func (sidebar *CueSidebar) changeToHelpContext() {
	sidebar.changeContext(sidebar.helpContextFactory())
}
