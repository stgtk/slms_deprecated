package CueMode

import (
	"fmt"
	"github.com/gizak/termui"
	"gitlab.com/stgtk/slms/pkg/Data"
)

type CueList struct {
	termui.List
	CurrentItemId  int
	SelectedItemId int
	Show           Data.Show
}

func newCueList() *CueList {
	list := &CueList{
		List:           *termui.NewList(),
		CurrentItemId:  0,
		SelectedItemId: 0,
		Show:           show,
	}
	list.Height = 30
	list.BorderLabel = "Cue List"
	return list
}

func (cueList *CueList) update() {
	cueList.renderList()
	termui.Render(defaultCueList)
}

func (cueList *CueList) renderList() {
	var result []string

	for index, cue := range cueList.Show.CueList {
		cueString := fmt.Sprintf("%s %s -> %s", cue.Number, cue.Description, cue.Trigger)

		if index == cueList.CurrentItemId {
			cueString = fmt.Sprintf("[%s](bg-green)", cueString)
		} else if index == cueList.SelectedItemId {
			cueString = fmt.Sprintf("[%s](fg-green)", cueString)
		}

		result = append(result, cueString)
	}

	cueList.Items = result
}

func (cueList *CueList) selectCueAbove() {
	if cueList.SelectedItemId > 0 {
		cueList.SelectedItemId -= 1
	}
	cueList.update()
}

func (cueList *CueList) selectCueBelow() {
	if cueList.SelectedItemId+1 < len(cueList.Items) {
		cueList.SelectedItemId += 1
	}
	cueList.update()
}

func (cueList *CueList) goToNextCue() {
	if cueList.CurrentItemId+1 < len(cueList.Items) {
		cueList.CurrentItemId += 1
		cueList.SelectedItemId = cueList.CurrentItemId
	}
	cueList.update()
}

func (cueList *CueList) goToPreviousCue() {
	if cueList.CurrentItemId > 0 {
		cueList.CurrentItemId -= 1
		cueList.SelectedItemId = cueList.CurrentItemId
	}
	cueList.update()
}

func (cueList *CueList) jumpToSelectedCue() {
	cueList.CurrentItemId = cueList.SelectedItemId
	cueList.SelectedItemId = cueList.CurrentItemId
	cueList.update()
}
