package main

import (
	"encoding/json"
	"github.com/AsynkronIT/protoactor-go/actor"
	"github.com/urfave/cli"
	"gitlab.com/stgtk/slms/misc/deprecated/Targets/MediaPlayer"
	"gitlab.com/stgtk/slms/misc/deprecated/tuiprecated/tui/ConfigMode"
	"gitlab.com/stgtk/slms/misc/deprecated/tuiprecated/tui/CueMode"
	"gitlab.com/stgtk/slms/pkg/Controller"
	"gitlab.com/stgtk/slms/pkg/Data"
	"io/ioutil"
	"os"
)

import log "github.com/sirupsen/logrus"

func newController() actor.Actor {
	mediaPlayer := MediaPlayer.MediaPlayer{}
	return &Controller.Controller{
		MediaPlayer: mediaPlayer.GetDefaultConfig(),
		Show:        Data.Show{},
	}
}

func main() {
	app := appFactory()
	err := app.Run(os.Args)
	if err != nil {
		log.Error(err)
	}

	/*
		log.SetLevel(logrus.TraceLevel)

		controllerProps := actor.FromProducer(newController)
		pid := actor.Spawn(controllerProps)
		pid.RequestFuture(Commands.TearUp{}, 30+time.Second).Result()

		// Sending some Commands
		pid.Tell(Commands.PreviousCue{})
		console.ReadLine()
	*/
}

func runConfigMode(c *cli.Context) error {
	ConfigMode.Run()
	return nil
}

func runCueMode(c *cli.Context) error {
	jsonInput, err := ioutil.ReadFile(c.Args().First())
	if err != nil {
		return err
	}

	inputShow := Data.Show{}
	err = json.Unmarshal(jsonInput, &inputShow)
	if err != nil {
		return err
	}

	err = CueMode.Run(inputShow)
	return nil
}

func generateShowFile(c *cli.Context) error {
	show := Data.Show{}
	showJson, err := json.Marshal(show.GetExampleData())
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(c.Args().First(), showJson, 0666)
	if err != nil {
		return err
	}

	return nil
}
