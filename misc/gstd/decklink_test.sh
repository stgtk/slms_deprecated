#!/bin/bash

gstd-client pipeline_create decklink_pipe \
  decklinkvideosrc mode=11 device-number=0 connection=1 name=black ! \
  deinterlace ! videorate ! videoconvert ! \
  'video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, interlace-mode=interleaved, framerate=30/1, width=1920, height=1080' ! \
  queue ! mirror mode=top ! mirror ! \
  interpipesink name=decklink_src \
  sync=false async=false

gstd-client pipeline_create auto_sink \
  interpipesrc name=interpipesrc1 listen-to=decklink_src is-live=true allow-renegotiation=true enable-sync=true ! \
  queue ! \
  videoconvert ! \
  autovideosink async=false sync=false

gstd-client pipeline_play decklink_pipe
gstd-client pipeline_play auto_sink
