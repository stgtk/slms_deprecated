#!/bin/bash

gst-launch-1.0 videomixer name=mixer background=black sink_0::zorder=2 ! \
  video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, framerate=30/1, width=1920, height=1080 ! \
  videoconvert ! autovideosink \
  \
  videotestsrc is-live=true ! \
  video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, framerate=30/1, width=1920, height=1080 ! \
  videoscale ! video/x-raw, width=495, height=270 ! videobox left=-200 alpha=0.5 ! timeoverlay ! queue2 ! mixer. \
  \
  videotestsrc pattern=snow ! \
  "video/x-raw,format=AYUV,width=800,height=600,framerate=(fraction)30/1" ! \
  timeoverlay ! queue2 ! mixer.
