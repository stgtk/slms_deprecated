#!/bin/bash

gstd-client pipeline_create test_src_1 \
  videotestsrc ! \
  video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, framerate=30/1, width=1920, height=1080 ! \
  interpipesink name=src_1 \
  sync=true async=false

gstd-client pipeline_create test_src_2 \
  videotestsrc is-live=true pattern=ball ! \
  video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, framerate=30/1, width=1920, height=1080 ! \
  interpipesink name=src_2

gstd-client pipeline_create test_src_3 \
  videotestsrc is-live=true pattern=ball ! \
  video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, framerate=30/1, width=1920, height=1080 ! \
  interpipesink name=src_3


gstd-client pipeline_create effect_pipe_1 \
  interpipesrc name=effect_interpipesrc_1 listen-to=src_2 is-live=true allow-renegotiation=true ! \
  interpipesink name=effect_1

gstd-client pipeline_create pipeline_video_mixer \
  videomixer name=mixer background=black sink_0::xpos=800 ! \
  video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, framerate=30/1, width=1920, height=1080 ! interpipesink name=the_mix \
  \
  interpipesrc name=dsla listen-to=src_1 allow-renegotiation=true ! \
  videoscale ! video/x-raw, width=495, height=270 ! queue2 ! mixer. \
  \
  videotestsrc pattern=snow ! \
  "video/x-raw,format=AYUV,width=800,height=600,framerate=(fraction)30/1" ! \
  timeoverlay ! queue2 ! mixer.

gstd-client pipeline_create test \
  interpipesrc name=dsla listen-to=src_2 is-live=true allow-renegotiation=true ! videoconvert ! autovideosink

gstd-client pipeline_create auto_sink \
  interpipesrc name=interpipesrc1 listen-to=src_1 is-live=true allow-renegotiation=true enable-sync=true ! \
  queue ! \
  videoconvert ! \
  autovideosink async=false sync=false

gstd-client pipeline_play test_src_1
#gstd-client pipeline_play effect_pipe_1
#gstd-client pipeline_play test_src_2
#gstd-client pipeline_play test_src_3
gstd-client pipeline_play pipeline_video_mixer
#gstd-client pipeline_play test
gstd-client pipeline_play auto_sink

# gstd-client element_set auto_sink interpipesrc1 listen-to src_2
