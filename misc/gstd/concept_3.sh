#!/usr/bin/env bash

gstd-client pipeline_create p1 \
    videotestsrc is-live=true ! \
    "video/x-raw,format=RGBA,width=640,height=480,framerate=30/1" ! \
    glupload ! \
    queue max-size-buffers=3 leaky=downstream ! \
    interpipesink name=s1 sync=true async=false caps=video/x-raw\(memory\:GLMemory\),format=RGBA,width=640,height=480,framerate=30/1

gstd-client pipeline_create p2 \
    interpipesrc listen-to=s1 is-live=true format=time allow-renegotiation=true enable-sync=true caps=video/x-raw\(memory\:GLMemory\),format=RGBA,width=640,height=480,framerate=30/1 ! \
    "video/x-raw(memory:GLMemory),format=RGBA,width=640,height=480,framerate=30/1" ! \
    queue max-size-buffers=3 leaky=downstream ! \
    glimagesink

# gstd-client debug_enable true
# gstd-client debug_threshold 4
gstd-client pipeline_play p1
gstd-client pipeline_play p2
