#!/bin/bash

gst-launch-1.0 -v videotestsrc pattern=snow ! \
  "video/x-raw,format=BGRA,width=800,height=600,framerate=(fraction)30/1" ! \
  timeoverlay ! \
  interpipesink name=src_test forward-events=true \
  \
  interpipesrc name=interpipesrc1 listen-to=src_test is-live=true format=time ! \
  videomixer name=mixer background=black sink_0::xpos=800 ! \
  video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, framerate=30/1, width=1920, height=1080 ! \
  queue ! videoconvert ! autovideosink
