#!/bin/bash

#gst-launch-1.0 -v filesrc location=machine_lullaby.ogg ! oggdemux ! vorbisdec ! audioconvert ! audioresample ! autoaudiosink videotestsrc ! video/x-raw, pixel-aspect-ratio=1/1, format=BGRA, framerate=30/1, width=1920, height=1080 ! timeoverlay shaded-background=true font-desc="Sans, 24" ! rsvgoverlay location=logo.svg x=1280 y=30 ! videoconvert ! autovideosink

gstd-client pipeline_create default_test_src \
  filesrc location=machine_lullaby.ogg ! oggdemux ! vorbisdec ! audioconvert ! audioresample ! autoaudiosink  videotestsrc ! video/x-raw, pixel-aspect-ratio=1/1, format=BGRA, framerate=30/1, width=1920, height=1080 ! timeoverlay shaded-background=true font-desc=\"Sans, 24\" ! rsvgoverlay location=logo.svg x=1745 y=30 ! videoconvert ! autovideosink
  videotestsrc ! video/x-raw, pixel-aspect-ratio=1/1, format=I420, framerate=30/1, width=1920, height=1080 ! autovideosink

gstd-client pipeline_play default_test_src
