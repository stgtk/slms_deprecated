---
  title: GStreamer Daemon API Reference
  author: RidgeRun
  template: /home/irvin/Templates/pandoc.tex
---

# High Level API
## Pipelines
### pipeline_create

- _pipeline_create \<name\> \<description\>_
- Creates a new pipeline named after name using the description gst-launch syntax.
- Parameters
    - name: name of the pipeline to create.
    - description: pipeline with gst-launch syntax.
- Returns, a new code
    - 0 «success»
    - 2 «Bad pipeline description»
    - 9 «Cannot update this resource»
    - 10 «Bad command»

### pipeline_play

- _pipeline_play \<name\>_
- Puts the pipeline named name in the PLAYING state.
- Parameters
    - name: name of the pipeline to create.
- Returns a new code
    - 0 «success»
    - 2 «Bad pipeline description»
    - 9 «Cannot update this resource»
    - 10 «Bad command»

### pipeline_pause

- _pipeline_pause \<name\>_
- Puts the pipeline named name in the PAUSE state.
- Parameters
    - name: name of the pipeline to create.
- Returns a new code
    - 0 «success»
    - 2 «Bad pipeline description»
    - 9 «Cannot update this resource»
    - 10 «Bad command»

### pipeline_stop

- _pipeline_stop \<name\>_
- Puts the pipeline named name in the NULL state.
- Parameters
    - name: name of the pipeline to create.
- Returns a new code
    - 0 «success»
    - 2 «Bad pipeline description»
    - 9 «Cannot update this resource»
    - 10 «Bad command»

### pipeline_delete

- _pipeline_delete \<name\>_
- Puts the pipeline named name, stopping it first if necessary.
- Parameters
    - name: name of the pipeline to create.
- Returns a new code
    - 0 «success»
    - 2 «Bad pipeline description»
    - 9 «Cannot update this resource»
    - 10 «Bad command»


### list_pipelines

- _list_pipelines_
- List the pipelines in the session.
- Returns a new code
    - 0 «success»


## Elements
### element_get

- _element_get \<name\> \<element\>\_\<property\>_
- Reads the value of the property of the element in pipeline.
- Parameters
    - name: name of the pipeline.
    - element: name of the element in the pipeline.
    - property: property of the element to read.
- Returns a new code
    - 0 «success»
    - 10 «Bad command»

### element_set

- _element_set \<name\> \<element\>\_\<property\>_
- Sets the value of the property of the element in pipeline.
- Parameters
    - name: name of the pipeline.
    - element: name of the element in the pipeline.
    - property: property of the element to set.
- Returns a new code
    - 0 «success»
    - 9 «Cannot update this resource»
    - 10 «Bad command»
    - 13 «Bad value»

### list_elements

- _list_elements \<name\>_
- List the elements in the pipelines in the session.
- Parameters
    - name: name of the pipeline.
- Returns a new code
    - 0 «success»
    - 10 «Bad command»

### list_properties

- _list_properties \<name\> \<element\>_
- List the properties in the element of the pipeline.
- Parameters
    - name: name of the pipeline.
    - element: name of the element in the pipeline.
- Returns a new code
    - 0 «success»
    - 10 «Bad command»


## Debug
### debug_enable

- _debug_enable \<value\>_
- Enable or Disable the debug.
- Parameters
    - bool value: value to enable or disable the debug (true/false).
- Returns a new code
    - 0 «success»
    - 13 «Bad value»


### debug_threshold

- _debug_threshold \<threshold\>_
- Define the level of the debug.
- Parameters
    - threshold: value of the level of debug.
- Returns a new code
    - 0 «success»


### debug_color

- _debug_color \<color\>_
- Enable or Disable the color on the debug log.
- Parameters
    - bool color: value to enable or disable the color in the debug (true/false).
- Returns a new code
    - 0 «success»
    - 13 «Bad value»

## Bus
### bus_read

- _bus_read \<name\>_
- Read the bus messages in the pipeline name . (Bus example)
- Parameters
    - name: name of the pipeline.
- Returns a new code
    - 0 «success»
    - 10 «Bad command»


### bus_filter

- _bus_filter \<name\>\_\<filter\>
- Filter the bus messages in the pipeline name . (Bus example)
- Parameters
    - name: name of the pipeline.
    - filter: name of the filter of the bus in the pipeline.
- Returns a new code
    - 0 «success»
    - 10 «Bad command»
    - 13 «Bad value»


### bus_timeout

- _bus\_timeout \<name\>\_\<time_ns\>_
- Sets the timeout of the bus in the pipeline name . (Bus example)
- Parameters
    - name: name of the pipeline.
    - time_ns: time of the timeout in nanoseconds.
- Returns a new code
    - 0 «success»
    - 10 «Bad command»
    - 13 «Bad value»


## Events
### event_eos

- _event_eos \<name\>_
- Creates a new EOS event and sends it to pipeline name .
- Parameters
    - name: name of the pipeline.
- Returns a new code
    - 0 «success»
    - 10 «Bad command»


### event_flush_start

- _event_flush_start \<name\>_
- Creates a new flush start event and sends it to pipeline name .
- Parameters
    - name: name of the pipeline.
- Returns a new code
    - 0 «success»
    - 10 «Bad command»
    - 17 «Event error»



### event_flush_stop

- _event_flush_stop \<name\> \<reset\>_
- Creates a new flush stop event and sends it to pipeline name .
- Parameters
    - name: name of the pipeline.
    - reset: if time should be reset. Default value: true.
- Returns a new code
    - 0 «success»
    - 10 «Bad command»
    - 17 «Event error»



### event_seek

- _event_seek \<name\> \<rate\> \<flags\> \<format\> \<flags\> \<start-type\> \<start\> \<end-type\> \<end\>_
- Creates a new seek event and sends it to pipeline name .
- Parameters
    - name: name of the pipeline.
    - rate: The new playback rate. Default value: 1.0.
    - format: The format of the seek values. Default value: 3.
    - flags: The optional seek flags. Default value: 1.
    - start-type: The type and flags for the new start position. Default value: 1.
    - start: The value of the new start position. Default value: 0.
    - end-type: The type and flags for the new end position. Default value: 1.
    - end: The value of the new end position. Default value: -1.
- Returns a new code
    - 0 «success»
    - 10 «Bad command»
    - 13 «Bad value» 17 «Event error»


# Low Level API
## create

- _create \<URI\>\_\<description\>_
- A new Create call of the argument with the description.
- Parameters
    - URI: name of the parameter to create. example: _/pipelines/\<name\>/event_eos_
    - description: it can changes according to the context (pipeline or event)
- Returns a new code
    - 0 «success»
    - 2 «Bad pipeline description»
    - 9 «Cannot update this resource»
    - 10 «Bad command»


## read

- _read_\<URI\>_
- A new Read call of the argument.
- Parameters
    - URI: name of the parameter to read. example: _/pipelines/\<name\>/bus_
- Returns a new code
    - 0 «success»
    - 2 «Bad pipeline description»
    - 9 «Cannot update this resource»
    - 10 «Bad command»


## update

- _update \<URI\>\_\<description\>_
- A new Update call of the argument with the description.
- Parameters
    - URI: name of the parameter to update. example: _/debug/enable/_
    - description: new value to set the resource.
- Returns a new code
    - 0 «success»
    - 2 «Bad pipeline description»
    - 9 «Cannot update this resource»
    - 10 «Bad command»

## delete

- _delete \<URI\>_
- Delete the resouce given by the URI.
- Parameters
    - URI: name of the resource to delete. example: /pipelines/\<name\>/
- Returns a new code
    - 0 «success»
    - 2 «Bad pipeline description»
    - 9 «Cannot update this resource»
    - 10 «Bad command»
