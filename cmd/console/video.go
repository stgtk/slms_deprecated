package main

import (
	"bufio"
	"github.com/AsynkronIT/goconsole"
	"github.com/AsynkronIT/protoactor-go/actor"
	"github.com/urfave/cli"
	"gitlab.com/stgtk/slms/pkg/native"
	"gitlab.com/stgtk/slms/pkg/video"
	"os"
)

import log "github.com/sirupsen/logrus"

func videoGstdServer(c *cli.Context) error {
	log.SetLevel(log.DebugLevel)

	context := actor.EmptyRootContext()
	props := actor.PropsFromProducer(func() actor.Actor { return &video.Video{} })
	pid := context.Spawn(props)

	input := bufio.NewScanner(os.Stdin)
	input.Scan()

	pid.Stop()

	console.ReadLine()

	return nil
}

func videoNative(c *cli.Context) error {
	native.Run()

	return nil
}
