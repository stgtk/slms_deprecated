package main

import (
	"encoding/json"
	"github.com/urfave/cli"
	"gitlab.com/stgtk/slms/pkg/Data"
	"io/ioutil"
)

func appFactory() (app *cli.App) {
	app = cli.NewApp()
	app.Name = "slms_console"
	app.HelpName = "slms"
	app.Usage = "For using slms functions without a gui"
	app.Author = "Produktionskollektiv Stugütak Bern"
	app.Email = "info@stugütak.ch"
	app.Version = "0.1.0"

	app.Commands = []cli.Command{
		{
			Name:   "gen_showfile",
			Usage:  "Generates and saves a example showfile",
			Action: generateShowFile,
		},
		{
			Name:  "video",
			Usage: "Video functionality",
			Subcommands: []cli.Command{
				{
					Name:   "gstd",
					Action: videoGstdServer,
				},
				{
					Name: "native",
					Action: videoNative,
				},
			},
		},
		{
			Name:  "tcp",
			Usage: "Some tcp tests",
			Subcommands: []cli.Command{
				{
					Name:   "simple",
					Action: tcpAddAndRunSimplePipeline,
				},
			},
		},
	}

	return app
}

func generateShowFile(c *cli.Context) error {
	show := Data.Show{}
	showJson, err := json.Marshal(show.GetExampleData())
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(c.Args().First(), showJson, 0666)
	if err != nil {
		return err
	}

	return nil
}
