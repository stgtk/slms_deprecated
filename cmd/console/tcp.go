package main

import (
	"fmt"
	"github.com/urfave/cli"
	"net"
)

func tcpAddAndRunSimplePipeline(c *cli.Context) {
	tcpAddr, err := net.ResolveTCPAddr("tcp", "localhost:5000")
	if err != nil {
		panic(err)
	}

	writeToConnection(tcpAddr, "pipeline_create p1 videotestsrc ! autovideosink")
	writeToConnection(tcpAddr, "pipeline_play p1")

}

func writeToConnection(tcpAddr *net.TCPAddr, message string) {
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		panic(err)
	}

	_, err = conn.Write([]byte(message))
	if err != nil {
		panic(err)
	}

	reply := make([]byte, 4000)
	_, err = conn.Read(reply)

	if err != nil {
		panic(err)
	}

	fmt.Println(string(reply))
	conn.Close()
}
