package main

import (
	"github.com/AsynkronIT/protoactor-go/log"
	"os"
)

func main() {
	app := appFactory()
	err := app.Run(os.Args)
	if err != nil {
		log.Error(err)
	}
}
