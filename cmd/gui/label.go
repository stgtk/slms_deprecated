package main

import (
    "github.com/gotk3/gotk3/gtk"
    "log"
)

func newLabel(title string) (*gtk.Label) {
    label, err := gtk.LabelNew(title)
    if err != nil {
      log.Fatal("Unable to create button:", err)
    }

  	return label
}
