package main

import (
    "github.com/gotk3/gotk3/gtk"
    "log"
)


func setupTreeView() (*gtk.TreeView) {
  	treeView, err := gtk.TreeViewNew()
  	if err != nil {
  		log.Fatal("Unable to create tree view:", err)
  	}

  	treeView.AppendColumn(createColumn("Version", COLUMN_VERSION))
  	treeView.AppendColumn(createColumn("Feature", COLUMN_FEATURE))

  	return treeView
}

func createColumn(title string, id int) *gtk.TreeViewColumn {
  	cellRenderer, err := gtk.CellRendererTextNew()
  	if err != nil {
  		log.Fatal("Unable to create text cell renderer:", err)
  	}

  	column, err := gtk.TreeViewColumnNewWithAttribute(title, cellRenderer, "text", id)
  	if err != nil {
  		log.Fatal("Unable to create cell column:", err)
  	}

  	return column
}
