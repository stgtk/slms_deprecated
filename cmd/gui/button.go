package main

import (
    "github.com/gotk3/gotk3/gtk"
    "log"
)

func newButton(title string) (*gtk.Button) {
    btn, err := gtk.ButtonNewWithLabel(title)
    if err != nil {
      log.Fatal("Unable to create button:", err)
    }

  	return btn
}
