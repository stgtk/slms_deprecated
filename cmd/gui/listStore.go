package main

import (
    "github.com/gotk3/gotk3/glib"
    "github.com/gotk3/gotk3/gtk"
    "log"
)

func setupListStore() (*gtk.ListStore) {
    // Creating a list store. This is what holds the data that will be shown on our tree view.
    listStore, err := gtk.ListStoreNew(glib.TYPE_STRING, glib.TYPE_STRING)
    if err != nil {
      log.Fatal("Unable to create list store:", err)
    }

    return listStore
}

func addRow(listStore *gtk.ListStore, version, feature string) {
  	// Get an iterator for a new row at the end of the list store
  	iter := listStore.Append()

  	// Set the contents of the list store row that the iterator represents
  	err := listStore.Set(iter,
  		[]int{COLUMN_VERSION, COLUMN_FEATURE},
  		[]interface{}{version, feature})

  	if err != nil {
  		log.Fatal("Unable to add row:", err)
  	}
}
