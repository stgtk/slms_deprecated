Latenzmessungen
***************

Für die Entwicklung. Eine Sammlung von Latenzzeiten.


Canon Legria HF R17
===================

Via HDMI -> BlackMagic HDMI to SDI -> SDI -> DeckLink

0.172s, sehr schlechte Qualität ::

  gst-launch-1.0 \
    decklinkvideosrc mode=11 device-number=0 connection=1 name=black ! \
    videoconvert ! \
    autovideosink

0.172s ::

  gstd-client pipeline_create decklink_pipe \
    decklinkvideosrc mode=11 device-number=0 connection=1 name=black ! \
    deinterlace ! videorate ! videoconvert ! \
    'video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, interlace-mode=interleaved, framerate=30/1, width=1920, height=1080' ! \
    queue ! \
    interpipesink name=decklink_src \
    sync=false async=false

  gstd-client pipeline_create auto_sink \
    interpipesrc name=interpipesrc1 listen-to=decklink_src is-live=true allow-renegotiation=true enable-sync=true ! \
    queue ! \
    videoconvert ! \
    autovideosink async=false sync=false

Gleich wie Oben, aber mit alpha=0.4: 0.172s

Raspberry Pi
============

Via HDMI -> BlackMagic HDMI to SDI -> SDI -> DeckLink

0.103637s, mit ``current_time.py``. Messung überhaupt nicht aussagekräftig ::

  gstd-client pipeline_create decklink_pipe \
    decklinkvideosrc mode=13 device-number=1 connection=1 name=black ! \
    deinterlace ! videorate ! videoconvert ! \
    'video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, interlace-mode=interleaved, framerate=60/1, width=1920, height=1080' ! \
    queue ! \
    interpipesink name=decklink_src \
    sync=false async=false

  gstd-client pipeline_create auto_sink \
    interpipesrc name=interpipesrc1 listen-to=decklink_src is-live=true allow-renegotiation=true enable-sync=true ! \
    queue ! \
    videoconvert ! \
    autovideosink async=false sync=false


OpenGL Up- und Download
=======================

::

  Canon Legria HF R17
  --(HDMI)-->
  Blackmagic HDMI to SDI Converter
  --(SDI)-->
  Blackmagic DeckLink

Upload, dann Ausgabe auf glimagesink
------------------------------------

::

  gst-launch-1.0 -q decklinkvideosrc mode=11 device-number=0 connection=1 name=black ! \
    deinterlace ! videorate ! videoconvert ! \
    'video/x-raw, pixel-aspect-ratio=1/1, interlace-mode=interleaved, framerate=30/1, width=1920, height=1080' ! \
    glupload ! \
    glimagesink &


+-------+-----------+-------+
| Input | Video Out | Delta |
+=======+===========+=======+
| 666   | 433       | 233   |
+-------+-----------+-------+
| 666   | 466       | 200   |
+-------+-----------+-------+
| 666   | 433       | 233   |
+-------+-----------+-------+
| 633   | 433       | 200   |
+-------+-----------+-------+
| 633   | 433       | 200   |
+-------+-----------+-------+


Ein Upload, danach drei OpenGL Effekte
--------------------------------------

::

  gst-launch-1.0 decklinkvideosrc mode=11 device-number=0 connection=1 name=black ! \
    deinterlace ! videorate ! videoconvert ! \
    'video/x-raw, pixel-aspect-ratio=1/1, interlace-mode=interleaved, framerate=30/1, width=1920, height=1080' ! \
    glupload ! \
    gleffects_heat ! gleffects_glow ! gleffects_xpro ! \
    glimagesink


+-------+-----------+-------+
| Input | Video Out | Delta |
+=======+===========+=======+
| 401   | 172       | 229   |
+-------+-----------+-------+
| 317   | 89        | 228   |
+-------+-----------+-------+
| 1034  | 805       | 229   |
+-------+-----------+-------+
| 835   | 607       | 228   |
+-------+-----------+-------+
| 637   | 408       | 229   |
+-------+-----------+-------+
| 439   | 210       | 229   |
+-------+-----------+-------+

Für jeden OpenGL Effekt einen eigenen Upload
--------------------------------------------

::

  gst-launch-1.0 decklinkvideosrc mode=11 device-number=0 connection=1 name=black ! \
    deinterlace ! videorate ! videoconvert ! \
    'video/x-raw, pixel-aspect-ratio=1/1, interlace-mode=interleaved, framerate=30/1, width=1920, height=1080' ! \
    glupload gleffects_heat gldownload ! \
    glupload gleffects_glow gldownload ! \
    glupload gleffects_xpro ! \
    glimagesink


+-------+-----------+-------+
| Input | Video Out | Delta |
+=======+===========+=======+
| 733   | 500       | 233   |
+-------+-----------+-------+
| 733   | 500       | 233   |
+-------+-----------+-------+
| 400   | 166       | 234   |
+-------+-----------+-------+
| 433   | 233       | 200   |
+-------+-----------+-------+
| 1133  | 900       | 233   |
+-------+-----------+-------+
| 766   | 533       | 233   |
+-------+-----------+-------+
| 300   | 100       | 200   |
+-------+-----------+-------+
| 900   | 666       | 234   |
+-------+-----------+-------+
| 766   | 533       | 233   |
+-------+-----------+-------+
