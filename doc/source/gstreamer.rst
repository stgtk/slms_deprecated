GStreamer
*********

.. _gstreamer-decklink:

Input
=====

DeckLink
--------

Mit dem `decklinkvideosrc <https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-bad/html/gst-plugins-bad-plugins-decklinkvideosrc.html>`_ Plugin.

``mode``
    Der Mode gibt das Format des Inputs an. Was die einzelnen Zahlen bedeuten, kann mit ``gst-inpsect-1.0 decklinkvideosrc`` in Erfahrung gebracht werden. Für eine automatische Erkennung kann der Value ``0`` angegeben werden. Es empfiehlt sich aber, in der DeckLink Desktopapplikation das korrekte Format in Erfahrung zu bringen und die Source damit zu konfigurieren.
``device-number``
    Jeder Eingang auf der DeckLink Karte ist für gstreamer ein einzelnes Device. Bei einer DeckLink Karte mit vier SDI Ports, existieren also vier valide device-numbers.
``connection``
    Welcher Typ von Input (SDI, HDMI usw.). 1 ist SDI

Automatisiert
.............

Grundsätzliches Beispiel. So viel wie möglich automatisiert. ::

  gst-launch-1.0 \
    decklinkvideosrc mode=0 device-number=0 connection=1 name=black ! \
    videoconvert ! \
    xvimagesink sync=false


Canon Legria HF R17
...................

Zusammen mit Blackmagic HDMI to SDI Converter (``mode=11`` entspricht HD1080 50i). ::

  gst-launch-1.0 \
    decklinkvideosrc mode=11 device-number=0 connection=1 name=black


Video und Audio
...............

::

    gst-launch-1.0 \
      decklinkvideosrc device-number=${DEVICE} mode=0 ! \
      queue ! \
      autovideoconvert ! \
      deinterlace mode=2 ! \
      queue ! \
      avenc_mpeg2video bitrate=800000 ! \
      mpegvideoparse ! \
      mpegtsmux name=mux ! \
      filesink location=video.ts decklinkaudiosrc device-number=${DEVICE} audio-channels=8 ! \
      audioconvert ! \
      avenc_ac3 bitrate=640000 ! \
      ac3parse ! queue ! mux.


.. _gstreamer-file:

File Input
----------

Mit dem `filesrc <https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer-plugins/html/gstreamer-plugins-filesrc.html>`_ Plugin.

.. _gstreamer-test:

V4L2 Input
----------



Test Input
----------

Mit dem `videotestsrc <https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-videotestsrc.html>`_ Plugin.

.. _gstreamer-black:

Schwarzes Bild
..............

Schwarzes Bild. ::

  gst-launch-1.0 -v videotestsrc pattern=black ! video/x-raw,width=1280,height=720 ! autovideosink

Weitere Snippets
================

Effekte
-------

Videobox, Videomixer und Alpha (Transparenz). ::

  gst-launch-1.0 \
    videomixer name=mix ! videoconvert ! autovideosink \
    videotestsrc pattern="snow" ! "video/x-raw,width=640,height=480" ! alpha alpha=1.0 ! mix. \
    videotestsrc ! "video/x-raw,width=320,height=240" ! alpha alpha=1.0 ! videobox top=-80 left=-80 border-alpha=0 ! mix.

Timer
-----

Ein Timer, praktisch für die Messung von Verzögerungen. ::

  gst-launch-1.0 -v videotestsrc pattern=black ! \
    'video/x-raw, width=1280, height=720' ! \
    timeoverlay halignment=center valignment=center font-desc="Sans, 44" ! \
    autovideosink


Dokumentation Gstreamer
=======================

`Dokumentation <https://gstreamer.freedesktop.org/documentation/>`_
