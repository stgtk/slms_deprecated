package Controller

import (
	"github.com/AsynkronIT/protoactor-go/actor"
	"gitlab.com/stgtk/slms/pkg/Commands"
)

import log "github.com/sirupsen/logrus"

func (controller *Controller) Receive(context actor.Context) {
	switch msg := context.Message().(type) {
	case *actor.Started:
		log.WithField("actor", "controller").Trace("Has started")
	case *actor.Stopping:
		log.WithField("actor", "controller").Trace("Is stopping")
		log.Debug("Controller is stopping")
	case *actor.Stopped:
		log.WithField("actor", "controller").Trace("Has stopped")
		log.Debug("Controller has stopped")
	case *actor.Restarting:
		log.WithField("actor", "controller").Trace("Is restarting")
		log.Debug("Controller is restarting")

	case Commands.TearUp:
		log.WithField("actor", "controller").Trace("TearUp command received")
		controller.tearUp(context)

	case Commands.PreviousCue:
		log.WithField("actor", "controlcontext actor.Contextler").Debug("PreviousCue command received")
		controller.previousCueCommand()
	case Commands.NextCue:
		log.WithField("actor", "controller").Debug("NextCue command received")
		controller.nextCueCommand()
	case Commands.JumpToCue:
		log.WithField("actor", "controller").Debug("JumpToCue command received")
		controller.jumpToCueCommand(msg.Destination)
	case Commands.Black:
		log.WithField("actor", "controller").Debug("Black command received")
		controller.blackCommand()
	default:
		log.WithFields(log.Fields{
			"actor":            "controller",
			"received_message": msg,
		}).Warn("Actor received unspecified message")
	}
}
