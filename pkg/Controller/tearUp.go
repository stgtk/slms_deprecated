package Controller

import log "github.com/sirupsen/logrus"

import (
	"github.com/AsynkronIT/protoactor-go/actor"
	"gitlab.com/stgtk/slms/pkg/Commands"
	"time"
)

// INTERNALS

func (controller *Controller) tearUpInternal() {
	controller.currentCueIndex = 0
}

// MEDIA PLAYER

func (controller *Controller) newMediaPlayer() actor.Actor {
	return &controller.MediaPlayer
}

func (controller *Controller) tearUpMediaPlayer() {
	mediaPlayerProps := actor.FromProducer(controller.newMediaPlayer)
	controller.MediaPlayer.Pid = actor.Spawn(mediaPlayerProps)
	controller.MediaPlayer.Pid.RequestFuture(Commands.TearUp{}, 30*time.Second).Result()
}

func (controller *Controller) tearUp(context actor.Context) {
	controller.tearUpInternal()
	controller.tearUpMediaPlayer()
	log.WithField("actor", "controller").Debug("TearUp successful finished")
	context.Respond("ok")
}
