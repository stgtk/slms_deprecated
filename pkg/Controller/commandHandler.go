package Controller

import log "github.com/sirupsen/logrus"

func (controller *Controller) previousCueCommand() {
	if controller.currentCueIndex-1 < 0 {
		log.WithField("actor", "controller").Error("Already at the beginning of the Cue List")
		return
	}
	controller.currentCueIndex = -1
	controller.fireCue(controller.currentCueIndex)

}

func (controller *Controller) nextCueCommand() {
	if controller.currentCueIndex+2 > len(controller.Show.CueList) {
		log.WithField("actor", "controller").Error("Already at the end of the Cue List")
		return
	}
	controller.currentCueIndex = +1
	controller.fireCue(controller.currentCueIndex)
}

func (controller *Controller) jumpToCueCommand(destinationIndex int) {
	if destinationIndex < 0 && destinationIndex+1 > len(controller.Show.CueList) {
		log.WithField("actor", "controller").Error("The given Destination Cue Index is out of scope")
		return
	}
	controller.currentCueIndex = destinationIndex - 1
	controller.fireCue(destinationIndex)
}

func (controller *Controller) blackCommand() {
	log.WithField("actor", "controller").Debug("Black func")
}
