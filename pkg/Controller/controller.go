package Controller

import (
	"gitlab.com/stgtk/slms/misc/deprecated/Targets/MediaPlayer"
	"gitlab.com/stgtk/slms/pkg/Data"
)

import log "github.com/sirupsen/logrus"

//Controller tears up the targets given by the Config and runs sequentially the commands given in Show.
type Controller struct {
	MediaPlayer     MediaPlayer.MediaPlayer
	Show            Data.Show
	currentCueIndex int
}

func (Controller) GetDefaultConfig() Controller {
	mediaPlayer := MediaPlayer.MediaPlayer{}
	return Controller{
		MediaPlayer: mediaPlayer.GetDefaultConfig(),
	}
}

func (controller *Controller) fireCue(index int) {
	log.Debug("FireCue is not implemented yet")
}
