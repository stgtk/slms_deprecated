package Data

type Cue struct {
	Number      string
	Description string
	Trigger     string
}

type CueList []Cue

func (CueList) GetExampleDataMap() CueList {
	return CueList{
		Cue{
			Number:      "1.0",
			Description: "Lights out",
			Trigger:     "Everyone is set",
		},
		Cue{
			Number:      "2.0",
			Description: "CS @ 30%",
			Trigger:     "Music",
		},
		Cue{
			Number:      "3.0",
			Description: "+ DS @50",
			Trigger:     "Choir",
		},
		Cue{
			Number:      "4.0",
			Description: "Stage @ Full",
			Trigger:     "Music Ends",
		},
	}
}

/*
In the end, we need this functionality as well
type Cue struct {
	Target 		// Ex.: Multimedia Player, Coordinated playback system
	Properties 	// Target properties for cue (ex.: video file path)
}
*/
