package Data

import "gitlab.com/stgtk/slms/pkg/video"

type Show struct {
	Name      string
	CueList   CueList
	Pipelines []video.GstdPipeline
}

func (Show) GetExampleData() Show {
	cueList := CueList{}
	return Show{
		Name:    "Example Show",
		CueList: cueList.GetExampleDataMap(),
	}
}
