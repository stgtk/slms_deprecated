package video

type Video struct {
	Gstd Gstd
}

func (video *Video) onStarted() {
	video.Gstd.Start()
}

func (video *Video) onStopping() {
	video.Gstd.Stop()
}
