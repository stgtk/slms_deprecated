package video

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net"
	"os/exec"
	"time"
)

import log "github.com/sirupsen/logrus"

type Gstd struct {
	Command *exec.Cmd
	Port    int
	TCPAddr *net.TCPAddr
}

// STARTING

func (gstd *Gstd) Start() {
	gstd.startGstdProcess()
	gstd.initTcpAddressAndTestConnection()

	pipelines := InterpipePiplineFactory()
	gstd.PipelinesCreate(pipelines)
	gstd.PipelinesPlay(pipelines)
}

func (gstd *Gstd) startGstdProcess() {
	gstd.logEntry().Debug("About to start gstd process")

	if gstd.Port == 0 {
		gstd.Port = 5000
	}

	gstd.Command = exec.Command(
		"gstd",
		fmt.Sprintf("--base-port=%d", gstd.Port))
	err := gstd.Command.Start()

	if err != nil {
		gstd.logEntry().Error(err)
	} else {
		log.WithFields(log.Fields{
			"actor":  "video",
			"module": "gstd",
			"pid":    gstd.Command.Process.Pid,
			"port":   gstd.Port,
		}).Info("Gstd process successfully started")
	}
}

func (gstd *Gstd) initTcpAddressAndTestConnection() {
	var err error
	gstd.TCPAddr, err = net.ResolveTCPAddr("tcp", fmt.Sprintf("127.0.0.1:%d", gstd.Port))

	gstd.logEntry().Debug("About to test connect with gstd tcp socket")
	time.Sleep(100 * time.Millisecond)

	conn, err := net.DialTCP("tcp", nil, gstd.TCPAddr)
	conn.Close()

	if err != nil {
		gstd.logEntry().Error(err)
	} else {
		gstd.logEntry().Info("Successfully tested connection to the tcp socket")
	}
}

// RUNNING

func (gstd *Gstd) sendCommand(command string) (response ApiResponse) {
	conn, err := net.DialTCP("tcp", nil, gstd.TCPAddr)
	if err != nil {
		gstd.logEntry().Error(err)
	}

	gstd.logCommandEntry(command).Trace("About to send command to gstd")
	_, err = conn.Write([]byte(command))
	if err != nil {
		gstd.logEntry().Error(err)
	}

	reply := make([]byte, 4096)
	_, err = conn.Read(reply)

	if err != nil {
		gstd.logEntry().Error(err)
	}

	reply = bytes.Trim(reply, "\x00")
	json.Unmarshal(reply, &response)
	gstd.logCommandEntry(command).Debug(string(reply))

	conn.Close()

	return response
}

// STOPPING

func (gstd *Gstd) Stop() {
	gstd.logEntry().Debug("About to kill gstd process")

	err := gstd.Command.Process.Kill()
	gstd.Command.Process.Wait()

	if err != nil {
		gstd.logEntry().Error(err)
	} else {
		gstd.logEntry().Info("Gstd process successfully killed")
	}
}

// GENERAL

func (Gstd) logEntry() *log.Entry {
	return log.WithFields(log.Fields{
		"actor":  "video",
		"module": "gstd",
	})
}

func (Gstd) logCommandEntry(command string) *log.Entry {
	return log.WithFields(log.Fields{
		"actor":   "video",
		"module":  "gstd",
		"command": command,
	})
}
