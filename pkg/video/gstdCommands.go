package video

// PIPELINES

func (gstd Gstd) PipelineCreate(pipeline GstdPipeline) {
	gstd.apiPipelineCreate(pipeline.Name, pipeline.GstPipeline.Render())
}

func (gstd Gstd) PipelinesCreate(pipelines GstdPipelines) {
	for _, pipeline := range pipelines {
		gstd.PipelineCreate(pipeline)
	}
}

func (gstd Gstd) PipelinePlay(pipeline GstdPipeline) {
	gstd.apiPipelinePlay(pipeline.Name)
}

func (gstd Gstd) PipelinesPlay(pipelines GstdPipelines) {
	for _, pipeline := range pipelines {
		gstd.PipelinePlay(pipeline)
	}
}

func (gstd Gstd) PipelinePause(pipeline GstdPipeline) {
	gstd.apiPipelinePause(pipeline.Name)
}

func (gstd Gstd) PipelineStop(pipeline GstdPipeline) {
	gstd.apiPipelineStop(pipeline.Name)
}

func (gstd Gstd) PipelineDelete(pipeline GstdPipeline) {
	gstd.apiPipelineDelete(pipeline.Name)
}
