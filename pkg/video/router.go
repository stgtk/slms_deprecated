package video

import (
	"github.com/AsynkronIT/protoactor-go/actor"
)

import log "github.com/sirupsen/logrus"

func (video *Video) Receive(context actor.Context) {
	switch msg := context.Message().(type) {
	case *actor.Started:
		log.WithField("actor", "video").Debug("Actor started")
		video.onStarted()
	case *actor.Stopping:
		log.WithField("actor", "video").Debug("Actor stopping")
		video.onStopping()
	default:
		log.WithFields(log.Fields{
			"actor":   "video",
			"message": msg,
		}).Warn("Actor received unspecified message")
	}
}
