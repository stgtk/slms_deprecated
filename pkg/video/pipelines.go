package video

import "fmt"

// GST

type GstProperty struct {
	key   string
	value string
}

func (property *GstProperty) RenderGstLaunchPart() (part string) {
	return fmt.Sprintf("%s=%s", property.key, property.value)
}

type GstProperties []GstProperty

type GstElement interface {
	GetPlugin() string
	GetName() string
	RenderGstElement() (line string)
}

type GstPluginElement struct {
	Plugin        string
	Name          string
	GstProperties GstProperties
}

func (element GstPluginElement) GetName() string {
	return element.Name
}

func (element GstPluginElement) GetPlugin() string {
	return element.Plugin
}

func (element GstPluginElement) RenderGstElement() (line string) {
	var properties string

	for index, property := range element.GstProperties {
		if index == 0 {
			properties = property.RenderGstLaunchPart()
			continue
		}
		properties = fmt.Sprintf("%s %s", properties, property.RenderGstLaunchPart())
	}

	return fmt.Sprintf("%s name=%s %s", element.Plugin, element.Name, properties)
}

type GstCapsElement struct {
	Caps []string
}

type GstPipeline []GstElement

func (pipeline GstPipeline) Render() string {
	var elements string

	for index, element := range pipeline {
		if index == 0 {
			elements = element.RenderGstElement()
			continue
		}
		elements = fmt.Sprintf("%s ! %s", elements, element.RenderGstElement())
	}

	return elements
}

// GSTD

type GstdPipeline struct {
	Name        string
	GstPipeline GstPipeline
}

func (gstdPipeline GstdPipeline) GetFreeGstElementName(plugin string) (name string) {
	count := 0
	for _, element := range gstdPipeline.GstPipeline {
		if element.GetPlugin() == plugin {
			count++
		}
	}

	count++
	return fmt.Sprintf("%s_%s_%s", gstdPipeline.Name, plugin, count)
}

type GstdPipelines []GstdPipeline
