package video

func BasicPipelineFactory() (pipeline GstdPipeline) {
	return GstdPipeline{
		Name: "p1",
		GstPipeline: []GstElement{
			GstPluginElement{
				Plugin: "videotestsrc",
			},
			GstPluginElement{
				Plugin: "autovideosink",
			},
		},
	}
}

func InterpipePiplineFactory() (pipelines []GstdPipeline) {
	return []GstdPipeline{
		{
			Name: "p1",
			GstPipeline: GstPipeline{
				GstPluginElement{
					Plugin: "videotestsrc",
					Name:   "p1_videotestsrc_1",
				},
				GstPluginElement{
					Plugin: "glupload",
					Name:   "p1_glupload_1",
				},
				GstPluginElement{
					Plugin: "gleffects_laplacian",
					Name:   "p1_gleffects_laplacian_1",
				},
				GstPluginElement{
					Plugin: "gldownload",
					Name:   "p1_gldownload_1",
				},
				GstPluginElement{
					Plugin: "interpipesink",
					Name:   "p1_interpipesink_1",
					GstProperties: []GstProperty{
						{"forward-events", "true"},
						{"sync", "true"},
					},
				},
			},
		},
		{
			Name: "p2",
			GstPipeline: GstPipeline{
				GstPluginElement{
					Plugin: "interpipesrc",
					Name:   "p2_interpipesrc_1",
					GstProperties: []GstProperty{
						{"listen-to", "p1_interpipesink_1"},
						{"is-live", "true"},
						{"format", "time"},
					},
				},
				GstPluginElement{
					Plugin: "glupload",
					Name:   "p2_glupload_1",
				},
				GstPluginElement{
					Plugin: "glimagesink",
					Name:   "p2_glimagesink_1",
				},
			},
		},
	}
}
