package video

import "fmt"

type ApiResponse struct {
	code        int
	description string
	response    string
}

/*
 For more Information visit https://developer.ridgerun.com/wiki/index.php?title=GStreamer_Daemon_-_API_Reference#Elements
*/

// PIPELINES

/* Creates a new pipeline named after name using the description gst-launch syntax */
func (gstd *Gstd) apiPipelineCreate(name, description string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("pipeline_create %s %s", name, description))
}

/* Puts the pipeline named name in the PLAYING state */
func (gstd *Gstd) apiPipelinePlay(name string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("pipeline_play %s", name))
}

/* Puts the pipeline named name in the PAUSE state */
func (gstd *Gstd) apiPipelinePause(name string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("pipeline_pause %s", name))
}

/* Puts the pipeline named name in the NULL state */
func (gstd *Gstd) apiPipelineStop(name string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("pipeline_stop %s", name))
}

/* Puts the pipeline named name, stopping it first if necessary */
func (gstd *Gstd) apiPipelineDelete(name string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("pipeline_delete %s", name))
}

/* List the pipelines in the session */
func (gstd *Gstd) apiPipelineList(name string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("list_pipelines %s", name))
}

// ELEMENTS

/* Reads the value of the property of the element in pipeline */
func (gstd *Gstd) apiElementGet(name, element, property string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("element_get %s %s %s", name, element, property))
}

/* Sets the value of the property of the element in pipeline */
func (gstd *Gstd) apiElementSet(name, element, property string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("element_set %s %s %s", name, element, property))
}

/* List the elements in the pipelines in the session */
func (gstd *Gstd) apiListElements(name string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("list_elements %s", name))
}

/* List the properties in the element of the pipeline */
func (gstd *Gstd) apiListProperties(name, element string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("list_properties %s %s", name, element))
}

// DEBUG

/* Enable or Disable the debug */
func (gstd *Gstd) apiDebugEnable(value string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("debug_enable %s", value))
}

/* Define the level of the debug */
func (gstd *Gstd) apiDebugThreshold(threshold string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("debug_threshold %s", threshold))
}

/* Enable or Disable the color on the debug log */
func (gstd *Gstd) apiDebugColor(color string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("debug_color %s", color))
}

// BUS

/* Read the bus messages in the pipeline name */
func (gstd *Gstd) apiBusRead(name string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("bus_read %s", name))
}

/* Filter the bus messages in the pipeline name */
func (gstd *Gstd) apiBusFilter(name, filter string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("bus_filter %s %s", name, filter))
}

/* Sets the timeout of the bus in the pipeline name */
func (gstd *Gstd) apiBusTimeout(name, timeNs string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("bus_timeout %s %s", name, timeNs))
}

// EVENTS

/* Creates a new EOS event and sends it to pipeline name */
func (gstd *Gstd) apiEventEos(name string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("event_eos %s", name))
}

/* Creates a new flush start event and sends it to pipeline name */
func (gstd *Gstd) apiEventFlushStart(name string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("event_flush_start %s", name))
}

/* Creates a new flush stop event and sends it to pipeline name */
func (gstd *Gstd) apiEventFlushStop(name string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("event_flush_stop %s", name))
}

/* Creates a new seek event and sends it to pipeline name */
func (gstd *Gstd) apiEventSeek(name, rate, format, flags, startType, start, endType, end string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("event_seek %s %s %s %s %s %s %s %s", name, rate, format, flags, startType, start, endType, end))
}

// LOW LEVEL API

/* A new Create call of the argument with the description */
func (gstd *Gstd) apiCreate(uri, description string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("create %s %s", uri, description))
}

/* A new Read call of the argument */
func (gstd *Gstd) apiRead(uri string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("read %s", uri))
}

/* Delete the resource given by the URI */
func (gstd *Gstd) apiDelete(uri string) (response ApiResponse) {
	return gstd.sendCommand(fmt.Sprintf("delete %s", uri))
}
