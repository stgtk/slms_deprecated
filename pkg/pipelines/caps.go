package pipelines

import "fmt"

type GstCaps struct {
	Type       string
	Properties []GstProperty
}

func (gstCaps GstCaps) RenderElement() (result string) {
	result = gstCaps.Type
	for _, property := range gstCaps.Properties {
		result = fmt.Sprintf("%s, %s", result, property.Render())
	}
	result = fmt.Sprintf("\"%s\"", result)
	return result
}

type GstCapsTemplate struct {
	Type       []ValueWithDescription
	Properties []SimplePropertyWithDescription
}

func (GstCapsTemplate) Example() GstCapsTemplate {
	return GstCapsTemplate{
		Type: []ValueWithDescription{
			{"audio/*", ""},
			{"audio/x-cinepak", ""},
			{"audio/x-dv", ""},
			{"audio/x-flac", ""},
			{"audio/x-gsm", ""},
			{"audio/x-alaw", ""},
			{"audio/x-mulaw", ""},
			{"audio/x-mace", ""},
			{"audio/mpeg", ""},
			{"audio/x-qdm2", ""},
			{"audio/x-pn-realaudio", ""},
			{"audio/x-speex", ""},
			{"audio/x-vorbis", ""},
			{"audio/x-wma", ""},
			{"audio/x-paris", ""},
			{"audio/x-svx", ""},
			{"audio/x-nist", ""},
			{"audio/x-voc", ""},
			{"audio/x-ircam", ""},
			{"audio/x-w64", ""},
			{"video/*", ""},
			{"video/x-raw", ""},
			{"video/x-raw(memory:GLMemory)", ""},
			{"video/x-3ivx", ""},
			{"video/x-divx", ""},
			{"video/x-3ivx", ""},
			{"video/x-divx", ""},
			{"video/x-dv", ""},
			{"video/x-ffv", ""},
			{"video/x-h263", ""},
			{"h263version", ""},
			{"video/x-h264", ""},
			{"video/x-huffyuv", ""},
			{"video/x-indeo", ""},
			{"video/x-intel-h263", ""},
			{"video/x-jpeg", ""},
			{"video/mpeg", ""},
			{"video/x-msmpeg", ""},
			{"video/x-msvideocodec", ""},
			{"video/x-pn-realvideo", ""},
			{"video/x-rle", ""},
			{"video/x-svq", ""},
			{"video/x-tarkin", ""},
			{"video/x-theora", ""},
			{"video/x-vp3", ""},
			{"video/x-wmv", ""},
			{"video/x-xvid", ""},
			{"image/gif", ""},
			{"image/jpeg", ""},
			{"image/png", ""},
			{"image/tiff", ""},
			{"video/x-ms-asf", ""},
			{"video/x-msvideo", ""},
			{"video/x-dv", ""},
			{"video/x-matroska", ""},
			{"video/mpeg", ""},
			{"application/ogg", ""},
			{"video/quicktime", ""},
			{"application/vnd.rn-realmedia", ""},
			{"audio/x-wav", ""},
		},
		Properties: []SimplePropertyWithDescription{
			{"format", ""},
			{"channels", ""},
			{"layout", ""},
			{"block_align", ""},
			{"framed", ""},
			{"layer", ""},
			{"bitrate", ""},
			{"height", ""},
			{"framerate", ""},
			{"max-framerate", ""},
			{"views", ""},
			{"interlace-mode", ""},
			{"chroma-site", ""},
			{"colorimetry", ""},
			{"pixel-aspect-ratio", ""},
			{"h263version", ""},
			{"systemstream", ""},
			{"depth", ""},
			{"width", ""},
		},
	}
}
