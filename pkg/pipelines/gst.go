package pipelines

import "fmt"

type GstElement interface {
	RenderElement() string
}

type GstPlugin struct {
	Plugin        string
	Name          string
	GstProperties []GstProperty
}

type GstPluginsTemplate struct {

}

type GstProperty struct {
	key   string
	value string
}

func (gstProperty GstProperty) Render() (result string) {
	return fmt.Sprintf("%s=%s", gstProperty.key, gstProperty.value)
}

