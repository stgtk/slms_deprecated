package pipelines

import "fmt"

// GStreamer Value
type GstValue interface {
	RenderValue() string
	IsSet() bool
}

// A GStreamer Boolean (GstValue)
type GstBoolean struct {
	Value  bool
	WasSet bool
}

func (GstBoolean) New(value bool) GstBoolean {
	return GstBoolean{
		Value: value,
		WasSet: true,
	}
}

func (gstBoolean GstBoolean) RenderValue() string {
	if gstBoolean.Value {
		return "(boolean)true"
	}
	return "(boolean)false"
}

func (gstBoolean GstBoolean) IsSet() bool {
	return gstBoolean.WasSet
}

// A GStreamer Integer (GstValue)
type GstInteger struct {
	Value  int
	WasSet bool
}

func (GstInteger) New(value int) GstInteger {
	return GstInteger{
		Value: value,
		WasSet: true,
	}
}

func (gstInteger GstInteger) RenderValue() string {
	return fmt.Sprintf("(int)%d", gstInteger.Value)
}

func (gstInteger GstInteger) IsSet() bool {
	return gstInteger.WasSet
}

// A GStreamer Integer (GstValue)
type GstString string

func (gstString GstString) RenderValue() string {
	return fmt.Sprintf("(string)%s", gstString)
}

func (gstString GstString) IsSet() bool {
	return len(gstString) == 0
}

// A GStreamer Fraction (GstValue)
type GstFraction struct {
	Numerator   int
	Denominator int
}

func (fraction GstFraction) RenderValue() string {
	return fmt.Sprintf("(fraction)%d/%d", fraction.Numerator, fraction.Denominator)
}

// A Value, which has a list of valid values (for examples the type of caps)
type RestrictedValue interface {
	ValidValues() []string
}
