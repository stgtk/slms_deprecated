package pipelines

type ValueWithDescription struct {
	Value       string
	Description string
}

type SimplePropertyWithDescription struct {
	Key          string
	Description  string
}
